package fr.istic.gla.mmm.mmm_tp1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import fr.istic.gla.mmm.mmm_tp1.R;
import fr.istic.gla.mmm.mmm_tp1.model.Contact;

public class AfficheInfoContactActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affiche_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Contact contact = getIntent().getExtras().getParcelable("contact");

        TextView tVName = (TextView) findViewById(R.id.tVName);
        TextView tVBirthDate = (TextView) findViewById(R.id.tVBirthDate);
        TextView tVBirthTown = (TextView) findViewById(R.id.tVBirthTown);
        TextView tVBirthCounty = (TextView) findViewById(R.id.tVBirthCounty);
        TextView tVPhone = (TextView) findViewById(R.id.tVPhone);
        if (contact != null) {
            String name = contact.getFirstname() + " " + contact.getLastname();
            tVName.setText(name);
            tVBirthDate.setText(contact.getBirthDate());
            tVBirthTown.setText(contact.getBirthTown());
            tVBirthCounty.setText(contact.getBirthCounty());
            tVPhone.setText(contact.getPhone());
        }
    }

}
