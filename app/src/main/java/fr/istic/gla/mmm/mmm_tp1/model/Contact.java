package fr.istic.gla.mmm.mmm_tp1.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by lautre on 19/01/16.
 */
public class Contact implements Parcelable {

    private int id;
    private String firstname;
    private String lastname;
    private String birthTown;
    private String birthDate;
    private String birthCounty;
    private String phone;



    public Contact(String firstname, String lastname, String birthTown, String birthDate, String birthCounty, String phone) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthTown = birthTown;
        this.birthDate = birthDate;
        this.birthCounty = birthCounty;
        this.phone = phone;
    }

    private Contact(Parcel in) {
        String[] data = new String[6];
        in.readStringArray(data);

        this.firstname = data[0];
        this.lastname = data[1];
        this.birthTown = data[2];
        this.birthDate = data[3];
        this.birthCounty = data[4];
        this.phone = data[5];
    }

    public static final Parcelable.Creator<Contact> CREATOR =
            new Parcelable.Creator<Contact>() {
                public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }
                public Contact[] newArray(int size) {
            return new Contact[size];
        }
            };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[]{
                this.firstname,
                this.lastname,
                this.birthTown,
                this.birthDate,
                this.birthCounty,
                this.phone
        });
    }

    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    public String getFirstname() {
        return firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    public String getLastname() {
        return lastname;
    }

    public void setBirthTown(String birthTown) {
        this.birthTown = birthTown;
    }
    public String getBirthTown() {
        return birthTown;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }
    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthCounty(String birthCounty) {
        this.birthCounty = birthCounty;
    }
    public String getBirthCounty() {
        return birthCounty;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getPhone() {
        return phone;
    }




}
