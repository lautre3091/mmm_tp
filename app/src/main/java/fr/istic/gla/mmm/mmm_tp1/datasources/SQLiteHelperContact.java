
package fr.istic.gla.mmm.mmm_tp1.datasources;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SQLiteHelperContact extends SQLiteOpenHelper {

	public static final String TABLE_CONTACT = "contacts";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_FIRSTNAME = "firstname";
	public static final String COLUMN_LASTNAME = "lastname";
	public static final String COLUMN_BIRTH_TOWN = "birthTown";
	public static final String COLUMN_BIRTH_DATE = "birthDate";
	public static final String COLUMN_BIRTH_COUNTY = "birthCounty";
	public static final String COLUMN_PHONE = "phone";

	private static final String DATABASE_NAME = "contact.db";
	private static final int DATABASE_VERSION = 4;

	// Database creation sql statement
	private static final String DATABASE_CREATE = "create table "
			+ TABLE_CONTACT + "( "
			+ COLUMN_ID + " integer primary key autoincrement, "
			+ COLUMN_FIRSTNAME + " text not null, "
			+ COLUMN_LASTNAME + " text not null, "
			+ COLUMN_BIRTH_TOWN + " text not null, "
			+ COLUMN_BIRTH_DATE + " number not null, "
			+ COLUMN_BIRTH_COUNTY + " text not null, "
			+ COLUMN_PHONE + " number)";

	public SQLiteHelperContact(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(SQLiteHelperContact.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACT);
		onCreate(db);
	}

}
