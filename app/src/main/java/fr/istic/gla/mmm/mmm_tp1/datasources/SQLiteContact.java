package fr.istic.gla.mmm.mmm_tp1.datasources;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import fr.istic.gla.mmm.mmm_tp1.model.Contact;

public class SQLiteContact {

	// Database fields
	private SQLiteDatabase database;
	private SQLiteHelperContact dbHelper;
	private String[] allColumns = {
			SQLiteHelperContact.COLUMN_ID,
			SQLiteHelperContact.COLUMN_FIRSTNAME,
			SQLiteHelperContact.COLUMN_LASTNAME,
			SQLiteHelperContact.COLUMN_BIRTH_TOWN,
			SQLiteHelperContact.COLUMN_BIRTH_DATE,
			SQLiteHelperContact.COLUMN_BIRTH_COUNTY,
			SQLiteHelperContact.COLUMN_PHONE
	};

	public SQLiteContact(Context context) {
		dbHelper = new SQLiteHelperContact(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public Contact createContact(Contact contact) {
		ContentValues values = new ContentValues();

		values.put(SQLiteHelperContact.COLUMN_FIRSTNAME, contact.getFirstname());
		values.put(SQLiteHelperContact.COLUMN_LASTNAME, contact.getLastname());
		values.put(SQLiteHelperContact.COLUMN_BIRTH_DATE, contact.getBirthDate());
		values.put(SQLiteHelperContact.COLUMN_BIRTH_COUNTY, contact.getBirthCounty());
		values.put(SQLiteHelperContact.COLUMN_BIRTH_TOWN, contact.getBirthTown());

		long insertId = database.insert(SQLiteHelperContact.TABLE_CONTACT, null, values);
		Cursor cursor = database.query(
				SQLiteHelperContact.TABLE_CONTACT,
				allColumns,
				SQLiteHelperContact.COLUMN_ID + " = " + insertId,
				null,
				null,
				null,
				null
	);

		cursor.moveToFirst();
		Contact newContact = cursorToContact(cursor);

		cursor.close();
		return newContact;
	}

	public Cursor queryWithFilter(String[] projection,String selection,String[] selectionArgs){
		return database.query(true, SQLiteHelperContact.TABLE_CONTACT, projection, selection, selectionArgs, null, null, null, null);
	}

	public void deleteContact(Contact contact) {
		long id = contact.getId();
		System.out.println("Contact deleted with id: " + id);
		database.delete(
				SQLiteHelperContact.TABLE_CONTACT,
				SQLiteHelperContact.COLUMN_ID + " = " + id,
				null
		);
	}

	public List<Contact> getAllContacts() {
		List<Contact> contacts = new ArrayList<>();

		Cursor cursor = database.query(
				SQLiteHelperContact.TABLE_CONTACT,
				allColumns,
				null,
				null,
				null,
				null,
				null
		);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Contact contact = cursorToContact(cursor);
			contacts.add(contact);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		return contacts;
	}

	private Contact cursorToContact(Cursor cursor) {
		Contact contact = new Contact(
				cursor.getString(cursor.getColumnIndex(SQLiteHelperContact.COLUMN_FIRSTNAME)),
				cursor.getString(cursor.getColumnIndex(SQLiteHelperContact.COLUMN_LASTNAME)),
				cursor.getString(cursor.getColumnIndex(SQLiteHelperContact.COLUMN_BIRTH_TOWN)),
				cursor.getString(cursor.getColumnIndex(SQLiteHelperContact.COLUMN_BIRTH_DATE)),
				cursor.getString(cursor.getColumnIndex(SQLiteHelperContact.COLUMN_BIRTH_COUNTY)),
				cursor.getString(cursor.getColumnIndex(SQLiteHelperContact.COLUMN_PHONE))
		);
		contact.setId(cursor.getInt(cursor.getColumnIndex(SQLiteHelperContact.COLUMN_ID)));
		return contact;
	}
}
