package fr.istic.gla.mmm.mmm_tp1;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import fr.istic.gla.mmm.mmm_tp1.adapter.CursorAdapterCustom;
import fr.istic.gla.mmm.mmm_tp1.contentProvider.ContentProviderSQLiteContact;
import fr.istic.gla.mmm.mmm_tp1.datasources.SQLiteHelperContact;
import fr.istic.gla.mmm.mmm_tp1.model.Contact;


public class ListContactActivity extends AppCompatActivity {

    private ListView listView;
    private Cursor cursor;
    private CursorAdapterCustom cursorAdapterCustom;

    private static final Uri CONTACT_URI = ContentProviderSQLiteContact.CONTENT_URI;
    private static final int REQUEST_CODE_ADD_CONTACT = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_contact);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView = (ListView) findViewById(R.id.lVContacts);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor c = (Cursor) parent.getItemAtPosition(position);

                String firstname = cursor.getString(cursor.getColumnIndex(ContentProviderSQLiteContact.CONTACT_FIRSTNAME));
                String lastname = cursor.getString(cursor.getColumnIndex(ContentProviderSQLiteContact.CONTACT_LASTNAME));
                String birthCounty = cursor.getString(cursor.getColumnIndex(ContentProviderSQLiteContact.CONTACT_BIRTH_COUNTY));
                String birthDate = cursor.getString(cursor.getColumnIndex(ContentProviderSQLiteContact.CONTACT_BIRTH_DATE));
                String birthTown = cursor.getString(cursor.getColumnIndex(ContentProviderSQLiteContact.CONTACT_BIRTH_TOWN));
                String phone = cursor.getString(cursor.getColumnIndex(ContentProviderSQLiteContact.CONTACT_PHONE));

                Contact contact = new Contact(firstname,lastname,birthTown,birthDate,birthCounty,phone);

                Intent intent = new Intent(ListContactActivity.this,AfficheInfoContactActivity.class);
                intent.putExtra("contact", contact);
                startActivity(intent);
            }
        });

        cursor = getContentResolver().query(CONTACT_URI, null, null, null, null);
        cursorAdapterCustom = new CursorAdapterCustom(this,cursor,0);

        listView.setAdapter(cursorAdapterCustom);
    }

    public void addContact(View view) {
        Intent intent = new Intent(ListContactActivity.this, AddContactActivity.class);
        ListContactActivity.this.startActivityForResult(intent, REQUEST_CODE_ADD_CONTACT);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list_contact, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (!"".equals(newText)) {
                    cursor = getContentResolver().query(CONTACT_URI, null, null, null, null);
                } else {
                    String selection = SQLiteHelperContact.COLUMN_FIRSTNAME + " LIKE ?";
                    String[] selectionArgs = {newText + "%"};
                    cursor = getContentResolver().query(CONTACT_URI, null, selection, selectionArgs, null);
                }
                cursorAdapterCustom.changeCursor(cursor);
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (REQUEST_CODE_ADD_CONTACT == requestCode && RESULT_OK == resultCode){
            Contact contact = data.getExtras().getParcelable("contact");

            ContentValues contentValues = new ContentValues();

            if (contact != null) {
                contentValues.put(ContentProviderSQLiteContact.CONTACT_LASTNAME,contact.getLastname());
                contentValues.put(ContentProviderSQLiteContact.CONTACT_FIRSTNAME,contact.getFirstname());
                contentValues.put(ContentProviderSQLiteContact.CONTACT_BIRTH_DATE,contact.getBirthDate());
                contentValues.put(ContentProviderSQLiteContact.CONTACT_BIRTH_COUNTY,contact.getBirthCounty());
                contentValues.put(ContentProviderSQLiteContact.CONTACT_BIRTH_TOWN,contact.getBirthTown());
                contentValues.put(ContentProviderSQLiteContact.CONTACT_PHONE,contact.getPhone());
            }

            getContentResolver().insert(CONTACT_URI, contentValues);

            cursor = getContentResolver().query(CONTACT_URI, null, null, null, null);

            cursorAdapterCustom.changeCursor(cursor);
        }

    }
}
