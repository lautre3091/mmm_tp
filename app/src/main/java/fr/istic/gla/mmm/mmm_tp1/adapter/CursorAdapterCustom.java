package fr.istic.gla.mmm.mmm_tp1.adapter;


import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.istic.gla.mmm.mmm_tp1.contentProvider.ContentProviderSQLiteContact;
import fr.istic.gla.mmm.mmm_tp1.R;
import fr.istic.gla.mmm.mmm_tp1.model.Contact;

public class CursorAdapterCustom extends CursorAdapter {

    private LayoutInflater mInflater;


    public CursorAdapterCustom(Context context, Cursor c, int flags) {
        super(context, c, flags);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return mInflater.inflate(R.layout.item_contact, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        TextView name = (TextView) view.findViewById(R.id.item_contact_nom);
        String sName = cursor.getString(
                cursor.getColumnIndex(ContentProviderSQLiteContact.CONTACT_FIRSTNAME)
        ) + " " +  cursor.getString(
                cursor.getColumnIndex(ContentProviderSQLiteContact.CONTACT_LASTNAME)
        );
        name.setText(sName);

        TextView birthDate = (TextView) view.findViewById(R.id.item_contact_date_naissance);
        birthDate.setText(cursor.getString(cursor.getColumnIndex(ContentProviderSQLiteContact.CONTACT_BIRTH_DATE)));

        TextView birthTown = (TextView) view.findViewById(R.id.item_contact_ville_naissance);
        birthTown.setText(cursor.getString(cursor.getColumnIndex(ContentProviderSQLiteContact.CONTACT_BIRTH_TOWN)));

    }
}
