package fr.istic.gla.mmm.mmm_tp1.contentProvider;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Parcel;
import android.util.Log;

import java.util.List;
import java.util.Objects;

import fr.istic.gla.mmm.mmm_tp1.datasources.SQLiteContact;
import fr.istic.gla.mmm.mmm_tp1.model.Contact;

/**
 * Created by lautre on 22/01/16.
 */
public class ContentProviderSQLiteContact extends ContentProvider {

    private static SQLiteContact datasource;

    public static final String _ID = "_ID";
    public static final String CONTACT_LASTNAME = "lastname";
    public static final String CONTACT_FIRSTNAME = "firstname";
    public static final String CONTACT_BIRTH_TOWN = "birthTown";
    public static final String CONTACT_BIRTH_DATE = "birthDate";
    public static final String CONTACT_BIRTH_COUNTY = "birthCounty";
    public static final String CONTACT_PHONE = "phone";

    // This must be the same as what as specified as the Content Provider authority
    // in the manifest file.
    static final String AUTHORITY = "contentprovidercustom";

    public static final String PROVIDER_NAME = "contentprovidercustom";

    public static final Uri CONTENT_URI = Uri.parse("content://"+ PROVIDER_NAME);


    // sample data to show the ContentProvider principle

    @Override
    public boolean onCreate() {
        datasource = new SQLiteContact(getContext());
        datasource.open();

        return true;
    }

    @Override
    public String getType(Uri uri) {
        return ContentResolver.CURSOR_DIR_BASE_TYPE + '/' + "com.contact";
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if (selection != null && selectionArgs != null)
            return datasource.queryWithFilter(projection, selection, selectionArgs);
        else {

            MatrixCursor c = new MatrixCursor(new String[]{
                    _ID,
                    CONTACT_FIRSTNAME,
                    CONTACT_LASTNAME,
                    CONTACT_BIRTH_TOWN,
                    CONTACT_BIRTH_DATE,
                    CONTACT_BIRTH_COUNTY,
                    CONTACT_PHONE,
            });
            int row_index = 0;
            List<Contact> contactList = datasource.getAllContacts();
            for (Contact aData : contactList) {

                c.newRow()
                        .add(row_index)
                        .add(aData.getFirstname())
                        .add(aData.getLastname())
                        .add(aData.getBirthTown())
                        .add(aData.getBirthDate())
                        .add(aData.getBirthCounty())
                        .add(aData.getPhone());
                row_index++;
            }
            return c;
        }
    }

    @Override
    public int update(Uri uri, ContentValues contentvalues, String s, String[] as) {
        throw new UnsupportedOperationException(" To be implemented");
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        Contact contact = new Contact(
                (String) contentValues.get(ContentProviderSQLiteContact.CONTACT_FIRSTNAME),
                (String) contentValues.get(ContentProviderSQLiteContact.CONTACT_LASTNAME),
                (String) contentValues.get(ContentProviderSQLiteContact.CONTACT_BIRTH_TOWN),
                (String) contentValues.get(ContentProviderSQLiteContact.CONTACT_BIRTH_DATE),
                (String) contentValues.get(ContentProviderSQLiteContact.CONTACT_BIRTH_COUNTY),
                (String) contentValues.get(ContentProviderSQLiteContact.CONTACT_PHONE)
        );
        if (datasource != null) {
            datasource.createContact(contact);
        }
        else Log.d("Debug","datasources null");
        return uri;
    }

    @Override
    public int delete(Uri uri, String s, String[] as) {
        throw new UnsupportedOperationException(" To be implemented");
    }

}
