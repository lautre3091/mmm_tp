package fr.istic.gla.mmm.mmm_tp1;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;

import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import fr.istic.gla.mmm.mmm_tp1.model.Contact;

public class AddContactActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if (id == R.id.action_why) {
            Toast.makeText(getApplicationContext(),
                    "Because I'm BATMAN ! ", Toast.LENGTH_SHORT).show();
        }
        else if (id == R.id.action_clear) {
            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.layout_form);
            for(int i=0;i<linearLayout.getChildCount();i++) {
                View child = linearLayout.getChildAt(i);
                if (child instanceof EditText)
                    ((EditText) child).setText("");
            }
            Toast.makeText(getApplicationContext(),
                    "All data cleared ! ", Toast.LENGTH_SHORT).show();
        }
        else if (id == R.id.action_addPhone) {
            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.layout_form);

            TextView textView = (TextView)getLayoutInflater().inflate(R.layout.tvtemplate, null);

            textView.setText("Téléphone : ");
            linearLayout.addView(textView, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

            EditText editText = new EditText(this);
            editText.setInputType(InputType.TYPE_CLASS_PHONE);
            editText.setId(R.id.eTPhone);
            linearLayout.addView(editText, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

            item.setEnabled(false);
        }
        else if (id == R.id.action_wiki_town) {
            EditText eTBirthTown = (EditText) findViewById(R.id.eTBirthTown);
            String town = eTBirthTown.getText().toString();
            if (!town.equals("")) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://fr.wikipedia.org/wiki/" + town));
                startActivity(intent);
            }
            else
                Toast.makeText(getApplicationContext(), "la ville n'est pas renseignée", Toast.LENGTH_SHORT).show();

        }

        return super.onOptionsItemSelected(item);
    }


    public void validInfo(View view){
        Intent returnIntent = new Intent();

        EditText eTFirstname = (EditText) findViewById(R.id.eTFirstname);
        EditText eTLastname = (EditText) findViewById(R.id.eTLastname);
        EditText eTBirthTown = (EditText) findViewById(R.id.eTBirthTown);
        EditText eTBirthDate = (EditText) findViewById(R.id.eTBirthDate);
        Spinner sBirthCounty = (Spinner) findViewById(R.id.sBirthCounty);
        EditText eTPhone = (EditText) findViewById(R.id.eTPhone);

        String phone = null;
        if (eTPhone != null)
            phone = eTPhone.getText().toString();

        Contact contact = new Contact(
                eTFirstname.getText().toString(),
                eTLastname.getText().toString(),
                eTBirthTown.getText().toString(),
                eTBirthDate.getText().toString(),
                sBirthCounty.getSelectedItem().toString(),
                phone);

        returnIntent.putExtra("contact", contact);

        setResult(RESULT_OK, returnIntent);
        finish();
    }
}
